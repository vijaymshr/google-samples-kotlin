package com.vijay.googlesampleskotlin.location

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.vijay.googlesampleskotlin.application.MyApplication
import com.vijay.googlesampleskotlin.util.Constants.Companion.EVENT_LAST_LOCATION
import com.vijay.googlesampleskotlin.util.Constants.Companion.EVENT_LOCATION_UPDATE
import com.vijay.googlesampleskotlin.util.Constants.Companion.FASTEST_INTERVAL
import com.vijay.googlesampleskotlin.util.Constants.Companion.INTENT_FILTER_LAST_LOCATION
import com.vijay.googlesampleskotlin.util.Constants.Companion.INTENT_FILTER_LOCATION_UPDATE
import com.vijay.googlesampleskotlin.util.Constants.Companion.INTERVAL
import com.vijay.googlesampleskotlin.util.Constants.Companion.REQUEST_CHECK_SETTINGS
import org.json.JSONObject


class FusedLocationSingleton {

    private var fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MyApplication.getAppContext())
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var lastKnownLocation: Location? = null

    fun configRequestLocationUpdate() {

        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = INTERVAL.toLong()
        locationRequest.fastestInterval = FASTEST_INTERVAL.toLong()

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    // Update UI with location data
                    // ...
                    if (location != null) {
                        // send location in broadcast
                        val intent = Intent(INTENT_FILTER_LOCATION_UPDATE)
                        intent.putExtra(EVENT_LOCATION_UPDATE, location)
                        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent)
                    }
                }
            }
        }

    }

    companion object {
        private var mInstance: FusedLocationSingleton? = null
        fun getInstance(): FusedLocationSingleton {
            if (mInstance == null) {
                mInstance = FusedLocationSingleton()
            }
            return mInstance as FusedLocationSingleton
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }


    @SuppressLint("MissingPermission")
    fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    //TODO Mock user location
//    @SuppressLint("MissingPermission")
//    fun mockUserLocation(location: Location) {
//        fusedLocationClient.setMockMode(true)
//        fusedLocationClient.setMockLocation(location)
//    }

    fun checkUserLocationSettings(activity: Activity) {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)

        val client: SettingsClient = LocationServices.getSettingsClient(MyApplication.getAppContext())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
            // All location settings are satisfied. The client can initialize
            // location requests here.
            // ...
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(activity,
                            REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
            /**
             * get last available location
             * @return last known location
             */
    fun getLastLocation() {
        Log.e("aaaa", "sssss " + fusedLocationClient.locationAvailability)
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        lastKnownLocation = location
                        val jsonObject = JSONObject()
                        jsonObject.put("latitude", location.latitude)
                        jsonObject.put("longitude", location.longitude)
                        Log.e("location", "" + location.latitude + "    :    " + location.longitude)
                        val intent = Intent(INTENT_FILTER_LAST_LOCATION)
                        intent.putExtra(EVENT_LAST_LOCATION, location)
                        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent)
                    }
                }
    }
}