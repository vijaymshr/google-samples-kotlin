package com.vijay.googlesampleskotlin.util

class Constants {
    companion object {
        val LOCATION_REQUEST_CODE = 100
        val EVENT_LOCATION_UPDATE = "locationUpdate"
        val EVENT_LAST_LOCATION = "lastKnownLocation"
        val INTENT_FILTER_LOCATION_UPDATE = "intentFilterLocationUpdate"
        val INTENT_FILTER_LAST_LOCATION = "intentFilterLastLocation"
        val INTERVAL = 2000
        val FASTEST_INTERVAL = 2000
        val REQUEST_CHECK_SETTINGS = 101
    }
}