package com.vijay.googlesampleskotlin.application

import android.app.Application
import android.content.Context

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // set app context
        mContextApplication = applicationContext
    }

    /**
     * retrieve application context
     * @return Context
     */
    companion object {
        private var mContextApplication: Context? = null

        fun getAppContext(): Context {
            return mContextApplication!!
        }

    }

}