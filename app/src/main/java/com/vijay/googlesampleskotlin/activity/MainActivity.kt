package com.vijay.googlesampleskotlin.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import com.vijay.googlesampleskotlin.R
import com.vijay.googlesampleskotlin.location.FusedLocationSingleton
import com.vijay.googlesampleskotlin.util.Constants.Companion.EVENT_LAST_LOCATION
import com.vijay.googlesampleskotlin.util.Constants.Companion.EVENT_LOCATION_UPDATE
import com.vijay.googlesampleskotlin.util.Constants.Companion.INTENT_FILTER_LAST_LOCATION
import com.vijay.googlesampleskotlin.util.Constants.Companion.INTENT_FILTER_LOCATION_UPDATE
import com.vijay.googlesampleskotlin.util.Constants.Companion.LOCATION_REQUEST_CODE
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                AlertDialog.Builder(this)
                        .setTitle("Location permission")
                        .setMessage("You need the location permission for some things to work")
                        .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->

                            ActivityCompat.requestPermissions(this@MainActivity,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    LOCATION_REQUEST_CODE)
                        })
                        .create()
                        .show()

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_REQUEST_CODE)
            }
            return false
        } else {
            return true
        }
    }

    @SuppressLint("SetTextI18n", "NewApi")
    override fun onResume() {
        super.onResume()
        //configuring FusedLocationSingleton for all the location requests
        FusedLocationSingleton.getInstance().configRequestLocationUpdate()

        //Checking for location settings
        FusedLocationSingleton.getInstance().checkUserLocationSettings(this@MainActivity)

        if (checkLocationPermission()) {
            FusedLocationSingleton.getInstance().startLocationUpdates()
            FusedLocationSingleton.getInstance().getLastLocation()
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(INTENT_FILTER_LOCATION_UPDATE)
        intentFilter.addAction(INTENT_FILTER_LAST_LOCATION)
        LocalBroadcastManager.getInstance(this@MainActivity).registerReceiver(mLocationUpdated,
                intentFilter)
    }

    override fun onPause() {
        super.onPause()
        if (checkLocationPermission()) {
            FusedLocationSingleton.getInstance().stopLocationUpdates()
        }
        LocalBroadcastManager.getInstance(this@MainActivity).unregisterReceiver(mLocationUpdated)
    }

    private val mLocationUpdated = object : BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent?) {
            try {
                val location = intent?.getParcelableExtra<Location>(EVENT_LOCATION_UPDATE)
                if (location != null) {
                    locationUpdateLatLonTV.text = "Latitude: ${location.latitude} : Longitude: ${location.longitude}"
                }
                val lastKnownLocation = intent?.getParcelableExtra<Location>(EVENT_LAST_LOCATION)
                if (lastKnownLocation != null) {
                    latLongTV.text = "Latitude: ${lastKnownLocation.latitude} : Longitude: ${lastKnownLocation.longitude}"
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        FusedLocationSingleton.getInstance().configRequestLocationUpdate()
                        FusedLocationSingleton.getInstance().startLocationUpdates()
                    }
                } else {
                    // permission denied!
                }
                return
            }
        }
    }


}
