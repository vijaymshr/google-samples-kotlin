# Google Samples Kotlin
> Google Code Samples written in kotlin.

### This repository contains the FusedLocationApi Singleton class which will be helpful for everyone and is written in kotlin.

### Feel free to use it and create a pull request in order to make it more reliable.

## Coming Soon...
* Singleton class for google login and facebook login probably.
* Singleton clas for all the api calls with reactive java.
